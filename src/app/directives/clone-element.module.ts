import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CloneElementDirective } from './clone-element.directive';


@NgModule({
  declarations: [
    CloneElementDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CloneElementDirective
  ]
})
export class CloneElementModule {
}
