import { Directive, Input, OnChanges, SimpleChanges, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appCloneElement]'
})
export class CloneElementDirective implements OnChanges {
  @Input() appCloneElement: number | null | undefined;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.viewContainer.clear();

    if (this.appCloneElement == null) {
      return;
    }

    if (!Number.isFinite(this.appCloneElement)) {
      return;
    }

    if (this.appCloneElement < 1) {
      return;
    }

    for (let i = 1; i <= this.appCloneElement; i++) {
      this.viewContainer.createEmbeddedView(this.templateRef)
    }
  }
}
