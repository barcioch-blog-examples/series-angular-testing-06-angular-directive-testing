import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CloneElementModule } from './clone-element.module';
import { By } from '@angular/platform-browser';

describe('CloneElementDirective', () => {
  let fixture: ComponentFixture<TestComponent>;
  let component: TestComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        CloneElementModule
      ],
      declarations: [TestComponent],
    });

    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
  });

  describe('when component with directive is initialized', () => {

    it('should not render any element', () => {
      expect(getClones().length).toBe(0);
    });

    describe.each([
      {input: -1, inputText: '-1'},
      {input: -581, inputText: '-581'},
      {input: 0, inputText: '0'},
      {input: null, inputText: 'null'},
      {input: undefined, inputText: 'undefined'},
      {input: Infinity, inputText: 'Infinity'},
      {input: NaN, inputText: 'NaN'},
    ])('and $inputText value is passed', ({input}) => {
      beforeEach(() => {
        component.cloneNumber = input;
        fixture.detectChanges();
      });

      it('should not render any element', () => {
        expect(getClones().length).toBe(0);
      });
    });

    describe.each([
      {input: 1},
      {input: 99},
      {input: 19},
    ])('and $input value is passed', ({input}) => {
      beforeEach(() => {
        component.cloneNumber = input;
        fixture.detectChanges();
      });

      it(`should render ${ input } element(s)`, () => {
        expect(getClones().length).toBe(input);
      });
    });

    describe('and 5 value is passed', () => {
      beforeEach(() => {
        component.cloneNumber = 5;
        fixture.detectChanges();
      });

      it(`should render 5 elements`, () => {
        expect(getClones().length).toBe(5);
      });

      describe('and 9 value is passed', () => {
        beforeEach(() => {
          component.cloneNumber = 9;
          fixture.detectChanges();
        });

        it(`should render 9 elements`, () => {
          expect(getClones().length).toBe(9);
        });
      });

      describe('and undefined value is passed', () => {
        beforeEach(() => {
          component.cloneNumber = undefined;
          fixture.detectChanges();
        });

        it(`should not render any element`, () => {
          expect(getClones().length).toBe(0);
        });
      });
    });
  });

  const getClones = (): DebugElement[] => {
    return fixture.debugElement.queryAll(By.css('.clone'));
  }
});


@Component({
  selector: 'app-test',
  template: `
    <div class="clone" *appCloneElement="cloneNumber">I'm a clone</div>
  `,
})
export class TestComponent {
  cloneNumber: number | null | undefined;
}
