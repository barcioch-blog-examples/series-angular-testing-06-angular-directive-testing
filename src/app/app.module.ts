import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CloneElementModule } from './directives/clone-element.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CloneElementModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
